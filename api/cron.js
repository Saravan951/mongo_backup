const CronJob = require("cron").CronJob;
const Cron = require("./backup.js");

// AutoBackUp every day (at 00:00 )
new CronJob(
	"0 0 * * *",
	function () {
		Cron.dbAutoBackUp();
	},
	null,
	true,
	"Asia/Kolkata"
);
