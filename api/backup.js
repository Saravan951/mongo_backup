const fs = require("fs");
const _ = require("lodash");

const path = require("path");
const config = require("../config/config");
var backup = require("mongodb-backup");
var rimraf = require("rimraf");

// Concatenate root directory path with our backup folder.
const backupDirPath = path.join(__dirname, "database-backup");

const dbOptions = {
	user: config.DB_UserName,
	pass: config.DB_Password,
	host: config.DB_Host,
	port: config.DB_Port,
	database: config.DB_Name,
	autoBackup: true,
	removeOldBackup: true,
	keepLastDaysBackup: 7,
	autoBackupPath: backupDirPath,
};

// return stringDate as a date object.
exports.stringToDate = (dateString) => {
	return new Date(dateString);
};

// Auto backup function
exports.dbAutoBackUp = async () => {
	// check for auto backup is enabled or disabled
	if (dbOptions.autoBackup == true) {
		let date = new Date();
		let beforeDate, oldBackupDir, oldBackupPath;

		// Current date
		currentDate = this.stringToDate(date);
		let newBackupDir =
			currentDate.getFullYear() +
			"-" +
			(currentDate.getMonth() + 1) +
			"-" +
			currentDate.getDate() +
			" " +
			currentDate.getHours() +
			":" +
			(currentDate.getMinutes() < 10 ? "0" : "") +
			currentDate.getMinutes();

		// New backup path for current backup process
		let newBackupPath = dbOptions.autoBackupPath + "/" + newBackupDir;
		// check for remove old backup after keeping # of days given in configuration
		if (dbOptions.removeOldBackup == true) {
			beforeDate = _.clone(currentDate);
			// Substract number of days to keep backup and remove old backup
			beforeDate.setDate(beforeDate.getDate() - dbOptions.keepLastDaysBackup);
			// beforeDate.setMinutes(beforeDate.getMinutes() - dbOptions.keepLastDaysBackup);

			oldBackupDir =
				beforeDate.getFullYear() +
				"-" +
				(beforeDate.getMonth() + 1) +
				"-" +
				beforeDate.getDate() +
				" " +
				beforeDate.getHours() +
				":" +
				(beforeDate.getMinutes() < 10 ? "0" : "") +
				beforeDate.getMinutes();
			// old backup(after keeping # of days)
			oldBackupPath = dbOptions.autoBackupPath + "/" + oldBackupDir;
		}

		await backup({
			// uri:
			// 	"mongodb://user_iotwebplatform_app:IoT!w3B$p|#tF06M$!2#2#@192.168.0.8:27017/iot_web_platform_Dev", // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
			uri: config.ConnectionString,
			root: newBackupPath,
			tar: "dump.tar",
			callback: function (err) {
				if (err) {
					console.error(err);
				} else {
					if (dbOptions.removeOldBackup == true) {
						if (fs.existsSync(oldBackupPath)) {
							rimraf.sync(oldBackupPath, { recursive: true }, function () {
								console.log("done");
							});
						}
					}
				}
			},
		});
	}
};
