module.exports = {
	DB_UserName: process.env.DB_UserName,
	DB_Password: process.env.DB_Password,
	DB_Name: process.env.DB_Name,
	DB_Host: process.env.DB_Host,
	DB_Port: process.env.DB_Port,
	IsTestMode: process.env.IsTestMode,
	ConnectionString: process.env.ConnectionString,
};
