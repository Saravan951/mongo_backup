const path = require("path");

require("dotenv").config({
	path: path.join(__dirname, ".env"),
});
global.CronJob = require("../backup/api/cron.js");
global.__base = __dirname + "/";
var express = require("express");

var app = express();
const http = require("http");
const port = process.env.PORT || 5007;

const server = http.createServer(app);
server.listen(port, () => {
	console.log(`Api running on localhost: ${port}`);
});
// console.log(process.env);
